import { defineStore } from "pinia";
import auth from "@/services/auth";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";
import router from "@/router";

export const useAuthStore = defineStore("auth", () => {
  const messageStore = useMessageStore();
  const loadingStore = useLoadingStore();

  const isLogin = () => {
    const user = localStorage.getItem("user");
    if (user) {
      return true;
    }
    return false;
  };

  const login = async (username: string, password: string): Promise<void> => {
    loadingStore.isLoading = true;

    try {
      const res = await auth.login(username, password);
      localStorage.setItem("user", JSON.stringify(res.data.user));
      localStorage.setItem("token", res.data.access_token);
      router.push("/");
    } catch (e) {
      messageStore.showError("Username หรือ Password ไม่ถูกต้อง");
    }
    loadingStore.isLoading = false;
  };

  const logout = () => {
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    router.replace("/login");
  };

  return { login, logout };
});
